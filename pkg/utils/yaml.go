package utils

import (
	"compress/gzip"
	"fmt"
	"io"
	"math/rand"
	"os"
	"time"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

func OpenYamlFile(filePath string, obj interface{}, useGZip bool) error {
	file, err := os.Open(filePath)
	if err != nil {
		return errors.Wrapf(err, "failed to open file: '%s'", filePath)
	}
	defer file.Close()

	if useGZip {
		if err = ReadGZipYaml(file, obj); err != nil {
			return errors.Wrapf(err, "failed to read yaml from gzip file '%s'", filePath)
		}
	} else {
		if err = yaml.NewDecoder(file).Decode(obj); err != nil {
			return errors.Wrap(err, "failed to decode yaml data")
		}
	}

	return nil
}

func ReadGZipYaml(reader io.Reader, obj interface{}) error {
	gzReader, err := gzip.NewReader(reader)
	if err != nil {
		return errors.Wrap(err, "failed to initialize gzip on file")
	}
	defer gzReader.Close()

	if err = yaml.NewDecoder(gzReader).Decode(obj); err != nil {
		return errors.Wrap(err, "failed to decode yaml data")
	}

	return nil
}

func SaveYamlFile(filePath string, obj interface{}, useGZip bool) error {
	rand.Seed(time.Now().Unix())
	newFilePath := filePath + fmt.Sprintf("%d", rand.Int())

	{
		file, err := os.OpenFile(newFilePath, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
		if err != nil {
			return errors.Wrapf(err, "failed to create file: '%s'", filePath)
		}
		defer file.Close()

		if useGZip {
			if err = WriteGZipYaml(file, obj); err != nil {
				return errors.Wrapf(err, "failed to write gzip file: '%s'", filePath)
			}
		} else {
			if err = yaml.NewEncoder(file).Encode(obj); err != nil {
				return errors.Wrap(err, "failed to encode yaml data")
			}
		}
	}

	if err := os.Rename(newFilePath, filePath); err != nil {
		return errors.Wrapf(err, "failed to move temporary file '%s' to '%s'", newFilePath, filePath)
	}

	return nil
}

func WriteGZipYaml(writer io.Writer, obj interface{}) error {
	gzWriter := gzip.NewWriter(writer)

	if err := yaml.NewEncoder(gzWriter).Encode(obj); err != nil {
		return errors.Wrap(err, "failed to encode yaml data")
	}

	if err := gzWriter.Close(); err != nil {
		return errors.Wrap(err, "failed to close gzip stream")
	}

	return nil
}
