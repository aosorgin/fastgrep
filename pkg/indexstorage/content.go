package indexstorage

import (
	"crypto/md5"
	"encoding/hex"
	"os"
	"path"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/aosorgin/fastgrep/pkg/fastindex"
	"gitlab.com/aosorgin/fastgrep/pkg/utils"
)

type storageContent struct {
	StorageIndex int               `yaml:"index"`
	MTime        time.Time         `yaml:"mtime"`
	Folders      map[string]string `yaml:"folders"`
}

func getIndexedName(filePath string) string {
	filePathHash := md5.Sum([]byte(filePath))
	return hex.EncodeToString(filePathHash[:8])
}

type folder struct {
	contentPath string  `yaml:"-"`
	cfg         *Config `yaml:"-"`

	StorageIndex int               `yaml:"index"`
	MTime        time.Time         `yaml:"mtime"`
	Files        map[string]string `yaml:"files"`
}

func (f *folder) openIndexFile(filePath string) (*fastindex.FilesIndex, error) {
	var index fastindex.FilesIndex
	useGZip := path.Ext(filePath) == ".gzindex"
	if err := utils.OpenYamlFile(filePath, &index, useGZip); err != nil {
		return nil, errors.Wrapf(err, "failed to decode index file '%s'", filePath)
	}

	return &index, nil
}

func (f *folder) saveIndexFile(filePath string, index *fastindex.FilesIndex) error {
	useGZip := path.Ext(filePath) == ".gzindex"
	if err := utils.SaveYamlFile(filePath, index, useGZip); err != nil {
		return errors.Wrapf(err, "failed to encode index file '%s'", filePath)
	}

	return nil
}

func (f *folder) getIndex(filePath string) (*fastindex.Index, error) {
	indexPath, ok := f.Files[filePath]
	if !ok {
		return nil, errors.Wrapf(ErrNotFound, "index of file '%s' is not found", filePath)
	}

	filesIndex, err := f.openIndexFile(indexPath)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to open index of file '%s'", filePath)
	}

	index, ok := filesIndex.Files[filePath]
	if !ok {
		return nil, errors.Wrapf(ErrCorrupted, "index of file '%s' is not found in index file '%s'", filePath, indexPath)
	}

	return &index, nil
}

func (f *folder) addIndex(filePath string, index *fastindex.Index) error {
	indexPath := path.Join(path.Dir(f.contentPath), getIndexedName(filePath))
	if f.cfg.Storage.Compress {
		indexPath += ".gzindex"
	} else {
		indexPath += ".index"
	}

	filesIndex := fastindex.NewIndex()
	filesIndex.Files[filePath] = *index

	if err := f.saveIndexFile(indexPath, filesIndex); err != nil {
		return errors.Wrap(err, "failed to save index")
	}

	f.Files[filePath] = indexPath
	if err := f.save(); err != nil {
		return errors.Wrap(err, "failed to save folder content")
	}

	return nil
}

func (f *folder) updateIndex(filePath string, index *fastindex.Index) error {
	indexPath, ok := f.Files[filePath]
	if !ok {
		return f.addIndex(filePath, index)
	}

	filesIndex, err := f.openIndexFile(indexPath)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			filesIndex = fastindex.NewIndex()
		} else {
			return errors.Wrapf(err, "failed to open index of file '%s'", filePath)
		}
	}

	filesIndex.Files[filePath] = *index

	if err := f.saveIndexFile(indexPath, filesIndex); err != nil {
		return errors.Wrap(err, "failed to save index")
	}

	f.Files[filePath] = indexPath
	if err := f.save(); err != nil {
		return errors.Wrap(err, "failed to save folder content")
	}

	return nil
}

func (f *folder) save() error {
	f.StorageIndex += 1
	f.MTime = time.Now()
	if err := utils.SaveYamlFile(f.contentPath, &f, false); err != nil {
		return errors.Wrapf(err, "failed to save content of folder '%s'", f.contentPath)
	}

	return nil
}

// context -> folder -> files -> indexes

// Storage functions

func (s *Storage) getFolder(folderPath string) (*folder, error) {
	contentPath, ok := s.content.Folders[folderPath]
	if !ok {
		return nil, errors.Wrapf(ErrNotFound, "folder '%s' is not found", folderPath)
	}

	var folderContent folder
	if err := utils.OpenYamlFile(contentPath, &folderContent, false); err != nil {
		if errors.Is(err, os.ErrNotExist) {
			folderContent.Files = make(map[string]string)
		} else {
			return nil, errors.Wrapf(err, "failed to open content of folder '%s'", folderPath)
		}
	}

	folderContent.contentPath = contentPath
	folderContent.cfg = s.cfg
	return &folderContent, nil
}

func (s *Storage) addFolder(folderPath string) (*folder, error) {
	folder, err := s.getFolder(folderPath)
	if err == nil {
		return folder, nil
	} else if !errors.Is(err, ErrNotFound) {
		return nil, errors.Wrap(err, "failed to get folder")
	}

	contentPath := path.Join(path.Dir(s.contentPath), getIndexedName(folderPath), "content.yaml")
	if err := os.MkdirAll(path.Dir(contentPath), 0755); err != nil {
		return nil, errors.Wrapf(err, "failed to create directory: %s", contentPath)
	}

	s.content.Folders[folderPath] = contentPath
	if err := s.save(); err != nil {
		return nil, errors.Wrap(err, "failed to save content")
	}

	return s.getFolder(folderPath)
}

func (s *Storage) save() error {
	s.content.StorageIndex += 1
	s.content.MTime = time.Now()
	if err := utils.SaveYamlFile(s.contentPath, &s.content, false); err != nil {
		return errors.Wrapf(err, "failed to save content of folder '%s'", s.contentPath)
	}

	return nil
}
