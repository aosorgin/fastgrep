package indexstorage

import (
	"os"
	"path"

	"github.com/pkg/errors"
	"gitlab.com/aosorgin/fastgrep/pkg/fastindex"
	"gitlab.com/aosorgin/fastgrep/pkg/utils"
)

// Storage implements
type Storage struct {
	contentPath string
	content     storageContent
	cfg         *Config
}

// UpdateFile append\update index of file
func (s *Storage) UpdateFile(filePath string, index *fastindex.Index) error {
	folder, err := s.addFolder(path.Dir(filePath))
	if err != nil {
		return errors.Wrap(err, "failed to add index folder")
	}

	if err := folder.updateIndex(filePath, index); err != nil {
		return errors.Wrap(err, "failed to update index")
	}

	return nil
}

// GetFile returns index of file if exists
func (s *Storage) GetFile(filePath string) (*fastindex.Index, error) {
	folder, err := s.getFolder(path.Dir(filePath))
	if err != nil {
		return nil, errors.Wrap(err, "failed to get index folder")
	}

	index, err := folder.getIndex(filePath)
	if err != nil {
		return nil, errors.Wrapf(err, "index for file '%s' is not found", filePath)
	}

	return index, nil
}

// OpenStorage opens storage of indexes
func OpenStorage(cfg Config) (*Storage, error) {
	if err := os.MkdirAll(cfg.Storage.Path, 0755); err != nil {
		return nil, errors.Wrapf(err, "failed to create directory: %s", cfg.Storage.Path)
	}

	contentPath := path.Join(cfg.Storage.Path, "content.yaml")
	var content storageContent

	if err := utils.OpenYamlFile(contentPath, &content, false); err != nil {
		if errors.Is(err, os.ErrNotExist) {
			content.Folders = make(map[string]string)
		} else {
			return nil, errors.Wrapf(err, "failed to open index's content: %s", contentPath)
		}
	}

	return &Storage{
		contentPath: contentPath,
		content:     content,
		cfg:         &cfg,
	}, nil
}
