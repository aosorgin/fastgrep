package indexstorage

import "github.com/pkg/errors"

var (
	ErrNotFound  = errors.New("not found")
	ErrCorrupted = errors.New("corrupted")
)
