package indexstorage

import (
	"io"
	"os"
	"path"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

type StorageConfig struct {
	Path     string `yaml:"path"`
	Compress bool   `yaml:"compress"`
}

type Config struct {
	Storage StorageConfig `yaml:"storage"`
}

// updateWithEnv updates missing fields with enveron
func (c *Config) updateWithEnv() error {
	return nil
}

func (c *Config) updateWithDefaults() error {
	if c.Storage.Path == "" {
		homePath := os.Getenv("HOME")
		if homePath == "" {
			homePath = "."
		}
		c.Storage.Path = path.Join(homePath, ".fastindex")
	}

	return nil
}

// ReadConfigYaml reads application's configuration
func ReadConfigYaml(data io.Reader) (*Config, error) {
	var config Config
	if err := yaml.NewDecoder(data).Decode(&config); err != nil {
		return nil, errors.Wrap(err, "failed to decode yaml configuration")
	}

	if err := config.updateWithEnv(); err != nil {
		return nil, errors.Wrap(err, "failed to update configuration with environ")
	}

	if err := config.updateWithDefaults(); err != nil {
		return nil, errors.Wrap(err, "failed to update configuration with defaults")
	}

	return &config, nil
}

// ReadConfigFromFile reads application's configuration from file
func ReadConfigFromFile(filePath string) (*Config, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to open config file: '%s'", filePath)
	}
	defer file.Close()

	return ReadConfigYaml(file)
}

// DefaultConfig returns default configuration
func DefaultConfig() (*Config, error) {
	var config Config

	if err := config.updateWithEnv(); err != nil {
		return nil, errors.Wrap(err, "failed to update configuration with environ")
	}

	if err := config.updateWithDefaults(); err != nil {
		return nil, errors.Wrap(err, "failed to update configuration with defaults")
	}

	return &config, nil
}
