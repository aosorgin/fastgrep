package buildindex

import (
	"fmt"
	"os"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/aosorgin/fastgrep/pkg/fastindex"
)

const (
	timeFormat    = "2006-01-02T15:04:05.999"
	timeFormatLen = len(timeFormat)
)

type logIndexer struct {
	lastIndexedTime time.Time
	timeDelta       time.Duration
	index           fastindex.Index
}

func (i *logIndexer) Process(line []byte, offset int64) error {
	if len(line) < timeFormatLen {
		return nil
	}

	rawTime := string(line[:timeFormatLen])

	t, err := time.Parse(timeFormat, rawTime)
	if err != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "failed to parse time: %s", rawTime).Error())
	}

	if t.Sub(i.lastIndexedTime) >= i.timeDelta {
		i.index.Nodes = append(i.index.Nodes, fastindex.Node{
			Start:  t,
			Offset: offset,
		})
		i.lastIndexedTime = t
	}

	return nil
}

func (i *logIndexer) Completed() (*fastindex.Index, error) {
	return &i.index, nil
}

// NewLogIndexer returns log indexer
func NewLogIndexer(timeDelta time.Duration) Indexer {
	return NewReadLineIndexer(&logIndexer{
		timeDelta: timeDelta,
	})
}
