package buildindex

import (
	"bufio"
	"io"
	"os"
	"os/signal"

	"github.com/pkg/errors"
	"gitlab.com/aosorgin/fastgrep/pkg/fastindex"
)

// Indexer indexes stream
type Indexer interface {
	Index(reader io.Reader, offset int64, progress Progress) (*fastindex.Index, error)
}

type LineProcessor interface {
	Process(line []byte, offset int64) error
	Completed() (*fastindex.Index, error)
}

type Progress interface {
	Offset(offset int64)
}

type readLineIndexer struct {
	proc LineProcessor
}

func (i *readLineIndexer) Index(rawReader io.Reader, offset int64, progress Progress) (*fastindex.Index, error) {
	sigChan := make(chan os.Signal, 1)
	defer close(sigChan)

	signal.Notify(sigChan, os.Interrupt)

	reader := bufio.NewReader(rawReader)
	var err error
	for err == nil {
		select {
		case <-sigChan:
			index, indexErr := i.proc.Completed()
			if indexErr != nil {
				return nil, errors.Wrap(err, indexErr.Error())
			}
			return index, errors.New(os.Interrupt.String())
		default:
			var line []byte
			if line, err = reader.ReadBytes('\n'); err != nil {
				if err == io.EOF {
					if err = i.proc.Process(line, offset); err != nil {
						index, indexErr := i.proc.Completed()
						if indexErr != nil {
							return nil, errors.Wrap(err, indexErr.Error())
						}
						return index, errors.Wrap(err, "failed to process line")
					}
					offset += int64(len(line))
					goto Complete
				}

				return nil, err
			}

			if err = i.proc.Process(line, offset); err != nil {
				index, indexErr := i.proc.Completed()
				if indexErr != nil {
					return nil, errors.Wrap(err, indexErr.Error())
				}
				return index, errors.Wrap(err, "failed to process line")
			}

			offset += int64(len(line))
			progress.Offset(offset)
		}
	}

Complete:
	progress.Offset(offset)
	return i.proc.Completed()
}

// NewReadLineIndexer creates indexer with read-line logic
func NewReadLineIndexer(proc LineProcessor) Indexer {
	return &readLineIndexer{
		proc: proc,
	}
}
