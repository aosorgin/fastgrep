package fastindex

import (
	"errors"
	"io"
)

type rangeLimitedReader struct {
	reader io.ReadSeeker
	offset int64
	begin  int64
	end    int64
}

func (r *rangeLimitedReader) Read(p []byte) (n int, err error) {
	n, err = r.reader.Read(p)
	if err != nil {
		return 0, err
	}

	defer func() {
		r.offset += int64(n)
	}()

	left := int(r.end - r.offset)
	if left < n {
		return left, io.EOF
	}
	return n, nil
}

func (r *rangeLimitedReader) Seek(offset int64, whence int) (int64, error) {
	switch whence {
	case io.SeekStart:
		offset += r.begin
	case io.SeekCurrent:
		offset += r.offset
	case io.SeekEnd:
		offset += r.end
	}

	if offset < r.begin && offset > r.end {
		return r.offset - r.begin, errors.New("offset is out ouf range")
	}

	offset, err := r.reader.Seek(offset, io.SeekStart)
	if err != nil {
		return offset - r.end, err
	}

	return offset - r.end, nil
}
