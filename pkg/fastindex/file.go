package fastindex

import (
	"io"
	"os"
	"time"

	"github.com/pkg/errors"
)

type IndexedReader struct {
	file  *os.File
	end   int64
	index *Index
}

func (r *IndexedReader) TimeRanged(from, to *time.Time) (io.ReadSeeker, error) {
	var begin int64
	end := r.end

	if from != nil {
		for _, n := range r.index.Nodes {
			if n.Start.Before(*from) && begin < n.Offset {
				begin = n.Offset
			}
		}
	}

	if to != nil {
		for _, n := range r.index.Nodes {
			if n.Start.After(*to) && end > n.Offset {
				end = n.Offset
			}
		}
	}

	if end < begin {
		return nil, errors.New("end is before begin")
	}

	offset, err := r.file.Seek(begin, io.SeekStart)
	if err != nil {
		return nil, errors.New("failed to seek")
	}

	return &rangeLimitedReader{
		reader: r.file,
		begin:  begin,
		offset: offset,
		end:    end,
	}, nil
}

// Close closes indexed reader
func (r *IndexedReader) Close() error {
	return r.file.Close()
}

// OpenIndexedFile opens indexed reader
func OpenIndexedFile(filePath string, index *Index) (*IndexedReader, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to open file '%s'", filePath)
	}

	stat, err := file.Stat()
	if err != nil {
		return nil, errors.Wrapf(err, "failed to get information of file '%s'", filePath)
	}

	return &IndexedReader{
		file:  file,
		index: index,
		end:   stat.Size(),
	}, nil
}
