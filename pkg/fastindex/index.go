package fastindex

import (
	"time"
)

// Node stores information of log's index
type Node struct {
	Start  time.Time `yaml:"time"`
	Offset int64     `yaml:"offset"`
}

// Index stores index information of log file
type Index struct {
	Nodes []Node
}

// FilesIndex stores information of files' indexes
type FilesIndex struct {
	Files map[string]Index `yaml:"files"`
}

// NewIndex returns new index
func NewIndex() *FilesIndex {
	return &FilesIndex{
		Files: make(map[string]Index),
	}
}
