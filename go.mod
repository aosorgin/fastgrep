module gitlab.com/aosorgin/fastgrep

go 1.13

require (
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.1.3
	gopkg.in/yaml.v2 v2.4.0
)
