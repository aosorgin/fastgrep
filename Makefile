.PHONY: build-all-swagger

GIT_COMMIT=$(shell git log -1 --pretty=format:"%H")

init:
	# Install golangci-lint
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s v1.27.0

dep:
	go mod vendor

build-fastindex:
	go build -o bin/fastindex -mod vendor ./cmd/main.go

fastindex-example:
	go run -mod vendor ./cmd/main.go index example.log

fastindex-example-cat:
	go run -mod vendor ./cmd/main.go cat example.log --from-time 2021-03-11T17:11:56.329Z --to-time 2021-03-11T17:25:49.571Z

build-all: build-fastindex

build-all-swagger: generate-swagger build-all

fmt:
	bash -c 'diff -u <(echo -n) <(gofmt -l . |grep -v vendor)'

lint:
	go vet -mod vendor ./...
	./bin/golangci-lint run ./...

test:
	go test -mod vendor ./...
