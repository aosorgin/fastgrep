package fastindexcmd

import (
	"io"
	"os"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/aosorgin/fastgrep/pkg/buildindex"
	"gitlab.com/aosorgin/fastgrep/pkg/fastindex"
	"gitlab.com/aosorgin/fastgrep/pkg/indexstorage"
)

var indexCmd = &cobra.Command{
	Use:   "index [file_path]",
	Short: "Build index of file",
	Args:  cobra.MinimumNArgs(1),
	RunE:  indexFile,
}

var timeDelta int
var continueIndexing bool

func getLastNode(index *fastindex.Index) *fastindex.Node {
	var maxTime time.Time
	var res fastindex.Node
	for _, node := range index.Nodes {
		if node.Start.After(maxTime) {
			res = node
			maxTime = node.Start
		}
	}

	return &res
}

func indexFile(cmd *cobra.Command, args []string) error {
	filePath := args[0]
	file, err := os.Open(filePath)
	if err != nil {
		return errors.Wrapf(err, "failed to open file '%s'", filePath)
	}
	defer file.Close()

	storage, err := getStorage()
	if err != nil {
		return errors.Wrap(err, "failed to open storage")
	}

	fileInfo, err := file.Stat()
	if err != nil {
		return errors.Wrap(err, "failed to get file info")
	}

	progress := getProgress("Indexing...", fileInfo.Size())
	indexer := buildindex.NewLogIndexer(time.Duration(time.Second * time.Duration(timeDelta)))

	var oldIndex *fastindex.Index
	var lastNode *fastindex.Node
	var offset int64

	if continueIndexing {
		oldIndex, err = storage.GetFile(filePath)
		if err != nil {
			if errors.Is(err, indexstorage.ErrNotFound) {
				emptyIndex := fastindex.Index{}
				oldIndex = &emptyIndex
			} else {
				return errors.Wrap(err, "failed to get index")
			}
		}

		if lastNode = getLastNode(oldIndex); lastNode != nil {
			if offset, err = file.Seek(lastNode.Offset, io.SeekStart); err != nil {
				return errors.Wrap(err, "failed to seek file")
			}
		}
	}

	index, indexErr := indexer.Index(file, offset, progress)
	if index == nil && indexErr != nil {
		return errors.Wrapf(indexErr, "failed to build index of file '%s'", filePath)
	}

	if index != nil {
		// Merge indexes
		if continueIndexing {
			for _, node := range oldIndex.Nodes {
				if node.Start.Before(lastNode.Start) {
					index.Nodes = append(index.Nodes, node)
				}
			}
		}

		if err = storage.UpdateFile(filePath, index); err != nil {
			return errors.Wrap(err, "failed to store index")
		}
	}

	if indexErr != nil {
		return errors.Wrapf(indexErr, "failed to build index of file '%s'. Intermediate index is saved", filePath)
	}

	return nil
}
