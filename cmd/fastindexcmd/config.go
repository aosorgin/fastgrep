package fastindexcmd

import (
	"os"
	"path"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/aosorgin/fastgrep/pkg/indexstorage"
)

var (
	configPath string

	storageConfig *indexstorage.Config
	storage       *indexstorage.Storage

	timeFromStr, timeToStr string
	timeFrom, timeTo       *time.Time
)

func getConfig() (*indexstorage.Config, error) {
	if storageConfig != nil {
		return storageConfig, nil
	}

	if configPath == "" {
		homePath := os.Getenv("HOME")
		if homePath == "" {
			homePath = "."
		}
		configPath = path.Join(homePath, "fastindex.cfg")
	}

	var err error
	if storageConfig, err = indexstorage.ReadConfigFromFile(configPath); err != nil {
		if errors.Is(err, os.ErrNotExist) {
			storageConfig, err = indexstorage.DefaultConfig()
		}

		if err != nil {
			return nil, errors.Wrap(err, "failed to read config file")
		}
	}

	return storageConfig, nil
}

func getStorage() (*indexstorage.Storage, error) {
	if storage != nil {
		return storage, nil
	}

	cfg, err := getConfig()
	if err != nil {
		return nil, err
	}

	storage, err := indexstorage.OpenStorage(*cfg)
	return storage, err
}

func getFromTime() (*time.Time, error) {
	if timeFrom != nil {
		return timeFrom, nil
	}

	if timeFromStr == "" {
		return nil, nil
	}

	t, err := time.Parse(time.RFC3339, timeFromStr)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse from-time: %s", timeFromStr)
	}

	timeFrom = &t
	return timeFrom, nil
}

func getToTime() (*time.Time, error) {
	if timeTo != nil {
		return timeTo, nil
	}

	if timeToStr == "" {
		return nil, nil
	}

	t, err := time.Parse(time.RFC3339, timeToStr)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse to-time: %s", timeToStr)
	}

	timeTo = &t
	return timeTo, nil
}
