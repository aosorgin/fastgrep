package fastindexcmd

import (
	"github.com/spf13/cobra"
)

var RootCmd = &cobra.Command{
	Use: "fastindex",
}

func init() {
	RootCmd.PersistentFlags().StringVarP(&configPath, "config", "c", "", "Configuration file path")

	indexCmd.Flags().IntVar(&timeDelta, "time-delta", 1, "time delta to build index in seconds [default: 1]")
	indexCmd.Flags().BoolVar(&continueIndexing, "continue", false, "Continue to build index")

	catCmd.Flags().StringVar(&timeFromStr, "from-time", "", "Start reading from the time (RFC3339)")
	catCmd.Flags().StringVar(&timeToStr, "to-time", "", "Reading to the time (RFC3339)")

	RootCmd.AddCommand(indexCmd)
	RootCmd.AddCommand(catCmd)
}
