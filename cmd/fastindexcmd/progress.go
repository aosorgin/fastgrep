package fastindexcmd

import (
	"fmt"

	"gitlab.com/aosorgin/fastgrep/pkg/buildindex"
)

type cliProgress struct {
	prefix       string
	total        int64
	lastProgress float32
}

func (p *cliProgress) Offset(offset int64) {
	progress := 100. * float32(offset) / float32(p.total)
	if progress-p.lastProgress >= 0.01 {
		fmt.Printf("\r%s %0.2f%%  ", p.prefix, progress)
		p.lastProgress = progress
	}
}

func getProgress(prefix string, total int64) buildindex.Progress {
	return &cliProgress{
		prefix: prefix,
		total:  total,
	}
}
