package fastindexcmd

import (
	"io"
	"os"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/aosorgin/fastgrep/pkg/fastindex"
)

var catCmd = &cobra.Command{
	Use:   "cat [file_path]",
	Short: "Build index of file",
	Args:  cobra.MinimumNArgs(1),
	RunE:  catFile,
}

func catFile(cmd *cobra.Command, args []string) error {
	storage, err := getStorage()
	if err != nil {
		return errors.Wrap(err, "failed to open storage")
	}

	filePath := args[0]

	index, err := storage.GetFile(filePath)
	if err != nil {
		return errors.Wrap(err, "failed to get index from storage")
	}

	reader, err := fastindex.OpenIndexedFile(filePath, index)
	if err != nil {
		return errors.Wrap(err, "failed to open file")
	}
	defer reader.Close()

	fromTime, err := getFromTime()
	if err != nil {
		return err
	}

	toTime, err := getToTime()
	if err != nil {
		return err
	}

	scoperReader, err := reader.TimeRanged(fromTime, toTime)
	if err != nil {
		return errors.Wrap(err, "failed to open time ranged reader")
	}

	_, err = io.Copy(os.Stdout, scoperReader)
	if err != nil {
		return errors.Wrap(err, "failed to copy data to output")
	}

	return nil
}
