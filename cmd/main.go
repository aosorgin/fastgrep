package main

import "gitlab.com/aosorgin/fastgrep/cmd/fastindexcmd"

func main() {
	if err := fastindexcmd.RootCmd.Execute(); err != nil {
		panic(err)
	}
}
